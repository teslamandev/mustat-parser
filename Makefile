APP_PATH=./

.PHONY: run build clean test venv venv_dev help

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  run		to start app"
	@echo "  clean		to remove build and temp files"
	@echo "  venv		to create environment"
	@echo "  venv_dev	to create development environment"
	@echo "  test		to run tests"

run:
	pipenv run python mustat.py

build: clean
	pipenv run pyinstaller --onefile mustat.py

clean:
	rm -rf *.pyc
	rm -rf *.swp
	rm -rf __pycache__
	rm -rf *.spec
	rm -rf build/
	rm -rf dist/

test: clean
	pipenv run pytest --color=yes $(APP_PATH)

venv:
	pipenv --three lock
	pipenv --three sync

venv_dev:
	pipenv --three lock
	pipenv --three sync
	pipenv --three install --dev
