#! env python
# -*- coding: utf-8 -*-
__author__      = "Eugene Sobolev"
__telegram__    = "http://t.me/eugenesobolev"
__email__       = "devteslaman@gmail.com"
__version__     = "1.2"

import csv
import os
import random
import re
import sys
import tempfile
import urllib
from collections import deque, namedtuple
from concurrent.futures import ThreadPoolExecutor, as_completed
from typing import List, Set, Deque

import requests
from bs4 import BeautifulSoup
from progressbar import progressbar
from requests import ConnectionError, Timeout

SiteInfo = namedtuple("SiteInfo",
    "hostname pagerank alexa per_day per_week per_month per_year")
UserInput = namedtuple("UserInput",
    "output_dir input_urls raw_urls_len threads_num proxies_queue timeout")
Response = namedtuple("Response",
    "url html")


def main():
    ui = get_user_input()
    print_summary_message(ui)

    # Ask for start
    if not is_ready_to_start():
        print("Программа завершена.")
        sys.exit(0)

    results = start_checking(ui)
    domains, subdomains, results_404 = handle_responses(results)

    print_final_message(domains, subdomains, results_404, ui)


####################################################
#############    HANDLE USER INPUT    ##############
####################################################


def get_user_input() -> UserInput:
    # Creating output directory.
    output_dir = create_dir()

    # Loading urls to parse
    raw_urls = load_file("input.txt")

    # Leave only unique urls and clean it.
    input_urls = clean_urls(raw_urls)
    if not input_urls:
        sys.exit("Файл input.txt пуст. Программа завершена.")

    raw_urls_len = len(raw_urls)

    # Ask for input desired number of threads
    threads_num = input_threads_num()

    # Loading proxy from proxy.txt
    proxies_queue = None
    if is_proxy_need():
        proxies_queue = proxies_to_queue(load_file("proxy.txt"))

    # Setting timeout
    timeout = None
    if proxies_queue:
        timeout = input_proxy_timeout()

    return UserInput(output_dir, input_urls, raw_urls_len, threads_num, proxies_queue, timeout)


def create_dir() -> str:
    while True:
        output_dir_name = input("ВЫХОДНАЯ ПАПКА: ")
        try:
            os.makedirs(output_dir_name)
            break
        except FileExistsError:
            while True:
                choice = input(
                    "Папка {} уже существует, использовать её? (Y / N): ".format(output_dir_name))
                if choice in "yY":
                    print("Выбрана существующая папка {}. "
                          "Все файлы в ней будут перезаписаны.".format(output_dir_name))
                    return output_dir_name
                elif choice in "nN":
                    break
                else:
                    continue
        except PermissionError:
            print("Недостаточно прав для записи в выбранную папку.")
        except OSError:
            print("Невозможно создать папку с таким названием. Выберите другое.")
    return output_dir_name


def load_file(filename: str) -> List[str]:
    try:
        with open(filename) as file:
            items = [item.strip() for item in file.readlines()]
    except FileNotFoundError:
        sys.exit("Файл {} не найден. Программа завершена.".format(filename))
    return items


def clean_urls(urls_list: List[str]) -> Set[str]:
    cleaned_urls = set()
    for url in urls_list:
        url_obj = urllib.parse.urlparse(url)
        if not url_obj.scheme or not url_obj.hostname:
            continue
        cleaned_urls.add("{}://{}".format(url_obj.scheme, url_obj.hostname))
    return cleaned_urls


def input_threads_num() -> int:
    while True:
        try:
            threads_num = int(input("УСТАНОВИТЬ КОЛИЧЕСТВО ПОТОКОВ: "))
        except ValueError:
            continue
        if not is_positive(threads_num):
            print("Введите число > 0.")
            continue
        return threads_num


def is_proxy_need() -> bool:
    while True:
        choice = input("ЗАГРУЗИТЬ СПИСОК SOCKS5? (Y / N): ")
        if choice in "yY":
            return True
        elif choice in "nN":
            return False
        else:
            continue


def proxies_to_queue(proxies: List[str]) -> Deque[str]:
    if not proxies:
        return None
    queue = deque()
    pattern = re.compile(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,5}$")
    for p in proxies:
        if not pattern.match(p):
            continue
        if p.endswith((":80", ":81", ":8080", ":3218")):
            continue
        queue.append(p)
    print("SOCKS5 ЗАГРУЖЕНО: {}".format(len(queue)))
    return queue


def input_proxy_timeout() -> int:
    while True:
        try:
            timeout = int(input("ТАЙМАУТ: "))
        except ValueError:
            continue
        if not is_positive(timeout):
            print("Введите целое положительное число.")
            continue
        return timeout


def is_ready_to_start() -> bool:
    while True:
        choice = input("НАЧАТЬ ЗАДАЧУ (Y / N): ")
        if choice in "yY":
            return True
        elif choice in "nN":
            return False
        else:
            continue


####################################################
##################    PARSING    ###################
####################################################


def start_checking(ui: UserInput) -> List[Response]:
    responses = []
    useragents = [
        "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36",
        "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36",
        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1",
    ]
    headers = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.5",
        "Cache-Control": "no-cache",
        "Pragma": "no-cache",
        "Host": "www.mustat.com",
    }
    with ThreadPoolExecutor(max_workers=ui.threads_num) as pool:
        futures = [
            pool.submit(
                check_url, url, random.choice(useragents),
                headers, ui.proxies_queue, ui.timeout
            ) for url in ui.input_urls
        ]
        print("\nПолучаю данные с www.mustat.com...")
        for future in progressbar(as_completed(futures), max_value=len(futures)):
            responses.append(future.result())
    return responses


def check_url(url: str, useragent: str, headers: dict, proxies_queue: Deque, timeout: int = None) -> Response:
    mustat_search_url = "https://www.mustat.com/search"
    headers["User-Agent"] = useragent
    params = {"q": url}
    while True:
        p = None
        if not proxies_queue:
            proxies = {}
        else:
            try:
                p = proxies_queue.popleft()
            except IndexError:
                pass
        if p:
            proxies = {
                "http": "socks5://{}".format(p),
                "https": "socks5://{}".format(p),
            }
        try:
            #  Making request to mustat.com
            resp = requests.post(mustat_search_url, headers=headers,
                                 proxies=proxies, timeout=timeout,  data=params)
            if p:
                proxies_queue.appendleft(p)
            return Response(url, resp.text)
        except (ConnectionError, Timeout, TypeError):
            pass


def handle_responses(responses: List[Response]):
    results_404: List[str] = []
    domains: List[Response] = []
    subdomains: List[Response] = []

    print("\nОбрабатываю собранные данные...")
    for resp in progressbar(responses):
        parsed_response = parse(resp)
        if not parsed_response:
            results_404.append(resp.url)
        else:
            dots_count = parsed_response.hostname.count(".")
            if ((dots_count == 2) and ("://www." in parsed_response.hostname)) or dots_count == 1:
                domains.append(parsed_response)
            else:
                subdomains.append(parsed_response)
    domains.sort(key=lambda x: x.per_day, reverse=True)
    subdomains.sort(key=lambda x: x.per_day, reverse=True)
    results_404.sort()
    return domains, subdomains, results_404


def parse(res: Response) -> SiteInfo:
    seo_data = scrap_html(res.html)
    if not seo_data:
        return None
    return SiteInfo(res.url, *seo_data)


def scrap_html(html: str) -> tuple:
    soup = BeautifulSoup(html, "lxml")
    try:
        if soup.find("title").get_text() == "404":
            return None
        per_day, per_week, per_month, per_year = [
            int(child.string.replace(",", ""))
            for child in soup.find("td", {"class": ["first", "ico-visitors"]}).parent.findAll("td")[1:]
        ]
        pagerank = soup.find(
            "th", {"class": "ico-pagerank"}).parent.find("td").get_text()
        alexa = soup.find("th", {"class": "ico-alexa"}
                          ).parent.find("a").get_text().replace(",", "")
    except AttributeError:
        return None
    return pagerank, alexa, per_day, per_week, per_month, per_year


####################################################
##################    OUTPUT    ####################
####################################################


def print_summary_message(ui: UserInput):
    urls_length = len(ui.input_urls)
    end_verb, end_adj, end_noun = get_endings(urls_length)
    line = "-" * 70

    print("\n" + line)
    print("Загружен{0} {1} уникальн{2} ссыл{3} из {4}".format(
        end_verb,
        urls_length,
        end_adj,
        end_noun,
        ui.raw_urls_len,
    ))
    if ui.proxies_queue:
        print("SOCKS: {}".format(len(ui.proxies_queue)))
    print("Количество потоков: {}".format(ui.threads_num))
    if ui.timeout:
        print("Время ожидания: {}".format(ui.timeout))
    print("Рабочая директория: {}".format(ui.output_dir))
    print(line + "\n")


def print_final_message(domains: List, subdomains: List, results_404: List, ui: UserInput):
    print("\n-------------------------- РАБОТА ЗАВЕРШЕНА --------------------------\n")

    domains_csv_fn = to_csv("output.csv", domains, ui.output_dir)
    print("Данные доменов записаны в {}".format(domains_csv_fn))

    subdomains_csv_fn = to_csv("output2.csv", subdomains, ui.output_dir)
    print("Данные субдоменов записаны в {}".format(subdomains_csv_fn))

    urls_404_fn = write_404_to_txt("404.txt", results_404, ui.output_dir)
    print("Ссылки 404 записаны в {}".format(urls_404_fn))

    if ui.proxies_queue:
        is_proxies_written = write_proxies("proxy.txt", ui.proxies_queue)
        if is_proxies_written:
            print("Записано {} рабочих прокси в proxy.txt".format(len(ui.proxies_queue)))
        else:
            print("Невозможно записать список SOCKS в файл proxy.txt")


def to_csv(filename: str, payload: List, output_dir: str):
    while True:
        try:
            with open("{}/{}".format(output_dir, filename), "w", newline='') as file:
                writer = csv.writer(file, quoting=csv.QUOTE_MINIMAL)
                writer.writerow(["Domain", "PageRank", "Alexa",
                                 "Per day", "Per week", "Per month", "Per year"])
                writer.writerows(payload)
            return "{}/{}".format(output_dir, filename)
        except (FileNotFoundError, PermissionError, OSError):
            print("Директория {} не найдена либо недостаточно прав для записи. "
                  "Создаю временную директорию.".format(output_dir))
            output_dir = tempfile.mkdtemp()


def write_404_to_txt(filename: str, responses_404: List[str], output_dir: str):
    while True:
        try:
            with open("{}/{}".format(output_dir, filename), "w", newline=os.linesep) as file:
                for line in responses_404:
                    file.write(line + "\n")
            return "{}/{}".format(output_dir, filename)
        except (FileNotFoundError, PermissionError, OSError):
            print("Директория {} не найдена либо недостаточно прав для записи. "
                  "Создаю временную директорию.".format(output_dir))
            output_dir = tempfile.mkdtemp()


def write_proxies(filename: str, proxies_queue: deque):
    try:
        newline = os.linesep
        with open(filename, "w", newline=newline) as file:
            for item in proxies_queue:
                file.write(item + newline)
        return True  # proxies written
    except OSError:
        return False  # proxies not written


####################################################
###################    UTILS    ####################
####################################################


def get_endings(num: int) -> str:
    if num % 100 in {11, 12, 13, 14}:
        return "о", "ых", "ок"
    elif num % 10 in {5, 6, 7, 8, 9, 0}:
        return "о", "ых", "ок"
    elif num % 10 in {2, 3, 4}:
        return "ы", "ые", "ки"
    else:
        return "а", "ая", "ка"


def is_positive(number: int) -> bool:
    if isinstance(number, int) and number > 0:
        return True
    return False


if __name__ == '__main__':
    main()
